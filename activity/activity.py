name = "Monkey D. Luffy";
age = 19;
occupation = "Pirate";
favorite_movie = "One Piece";
rating = 100;


print (f"I am {name}, and I am {age} years old. I work as a {occupation}, and my rating for {favorite_movie} is {rating} %");

num1, num2, num3 = 3, 5, 10;
print (num1 * num2 * num3);
print (num1 < num3);
num3 += num2;
print (num3);